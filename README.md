# Requirements

Uses the [png++](https://www.nongnu.org/pngpp/) library for generating pngs.
If on arch it can be installed by `pacman`.

# Build

`make`

Then run `bin/mandel_gen`

# Usage

```
Usage: analyze_png [options] filename

Positional arguments:
filename     	png filename

Optional arguments:
-h --help    	shows help message and exits
-v --version 	prints version information and exits
--color      	base color for mandelbrot plot (ex. 0x33FFAB) [default: 255]
--end-color  	background color for mandelbrot plot (ex. 0x33FFAB) [default: 0]
-t           	threshold brightness for black (0 - 255) [default: 0]
-x           	output image X size [default: 1024]
-y           	output image Y size [default: 1024]
--cut        	Mandelbrot set cut iterations [default: 20]
--off-x      	X offset [default: 0]
--off-y      	Y offset [default: 0]
--scale      	image scale factor [default: 1]
```

# Examples

## Example 1

Command:

`mandel_gen test.png -x 1000 -y 1000 --scale 3 --off-x 1 --cut 1000 --color 0x00ff00`

Output:
![Example picture](example.png)

## Example 2

The valley with the seahorses.

Command:
`mandel_gen ~/test.png -x 1000 -y 1000 --scale 0.5 --off-x 0.9 --cut 1000 --off-y 0.25 --color 0x00ffaa -t 5`

Output:
![Example 2](example2.png)

## Example 3

Creepy...

Command:
`mandel_gen example3.png -x 1000 -y 1000 --scale 0.15 --off-x 0.9 --cut 100 --off-y 0.3 --color 0x000000 --end-color 0xffffff`

Output:
![Example 3](example3.png)
