#ifndef HELPERS
#define HELPERS

#include <string>

struct RGB {
    int r;
    int g;
    int b;
};

class Color {
private:
    double l;

public:
    Color(int r, int g, int b, double l);
    Color(int);
    Color(int, int);
    RGB int_to_color(int);
    RGB mix_colors(double);
    RGB main_color;
    RGB bgn_color;
};

#endif /* HELPERS */
