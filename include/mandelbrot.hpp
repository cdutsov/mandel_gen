#ifndef MANDELBROT
#define MANDELBROT

#include <complex>
#include <png++/png.hpp>
#include "in_out.hpp"
#include "helpers.hpp"

class Mandelbrot {
private:
    int m_limit = 10;
    int iter_to_diverge;
    std::complex<double> calculate_step(std::complex<double> z, std::complex<double> c);

public:
    Mandelbrot(int limit);
    bool is_finite(std::complex<double> c);
    int iters_to_diverge();
    png::image<png::rgb_pixel> gen_image(Config, Color);

};

#endif /* MANDELBROT */
