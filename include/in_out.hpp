#ifndef IN_OUT
#define IN_OUT

#include "argparse.hpp"

class Config {
public:
    Config(int, char*[]);
    int x_size;
    int y_size;
    double scale;
    int mandel_cut;
    std::tuple<double, double> offset;
    std::string filename;
    uint32_t color;
    uint32_t end_color;
    int black_threshold;
};

#endif /* IN_OUT */
