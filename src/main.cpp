#include "mandelbrot.hpp"
#include "in_out.hpp"
#include "helpers.hpp"

int main(int argc, char *argv[]) {
    Config config = Config(argc, argv);
    Mandelbrot m_set = Mandelbrot(config.mandel_cut);
    Color color = Color(config.color, config.end_color);

    auto image = m_set.gen_image(config, color);
    image.write(config.filename);

    return 0;
}

