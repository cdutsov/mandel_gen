#include "mandelbrot.hpp"
#include "math.h"
#include <iostream>

Mandelbrot::Mandelbrot(int limit) {
    m_limit = limit;
}

std::complex<double> Mandelbrot::calculate_step(std::complex<double> z, std::complex<double> c) {
    return std::pow(z, 2) + c;
}

bool Mandelbrot::is_finite(std::complex<double> c) {
    std::complex<double> z_new = 0;
    std::complex<double> z_old = 0;
    iter_to_diverge = m_limit;
    for (int i = 0; i < Mandelbrot::m_limit; i++) {
        z_old = z_new;
        z_new = calculate_step(z_old, c);
        if (std::abs(z_new) > 2.0) {
            iter_to_diverge = i;
            return false;
        };

    }
    return true;
}

int Mandelbrot::iters_to_diverge() {
    return iter_to_diverge;
}

png::image<png::rgb_pixel> Mandelbrot::gen_image(Config config, Color color) {
    png::image<png::rgb_pixel> image(config.x_size, config.y_size);
    for (uint32_t y = 0; y < image.get_height(); ++y) {
        for (uint32_t x = 0; x < image.get_width(); ++x) {
            double re = (double) x / (double) image.get_width() * config.scale - std::get<0>(config.offset) - config.scale / 2.0;
            double im = (double) y / (double) image.get_height() * config.scale - std::get<1>(config.offset) - config.scale / 2.0;
            auto c = std::complex<double>(re, im);
            if (is_finite(c)) {
                auto main_color = color.main_color;
                image[y][x] = png::rgb_pixel(main_color.r, main_color.g, main_color.b);
            } else {
                double mix = std::log(iters_to_diverge() - config.black_threshold)/std::log(config.mandel_cut);
                if (mix < 0.0) {
                    mix = 0.0;
                };
                if (mix > 1.0) {
                    mix = 1.0;
                };
                auto col = color.mix_colors(mix);
                image[y][x] = png::rgb_pixel(col.r, col.g, col.b);
            }
        }
    }
    return image;
}
