#include "in_out.hpp"

Config::Config(int argc, char* argv[]) {
    argparse::ArgumentParser program("analyze_png", "0.1");

    program.add_argument("filename")
    .help("png filename");

    program.add_argument("--color")
    .help("base color for mandelbrot plot (ex. 0x33FFAB)")
    .default_value((uint32_t) 255)
    .scan<'x', uint32_t>();

    program.add_argument("--end-color")
    .help("background color for mandelbrot plot (ex. 0x33FFAB)")
    .default_value((uint32_t) 0)
    .scan<'x', uint32_t>();

    program.add_argument("-t")
    .help("threshold brightness for black (0 - 255)")
    .default_value(0)
    .scan<'i', int>();

    program.add_argument("-x")
    .help("output image X size")
    .default_value(1024)
    .scan<'i', int>();

    program.add_argument("-y")
    .help("output image Y size")
    .default_value(1024)
    .scan<'i', int>();

    program.add_argument("--cut")
    .help("Mandelbrot set cut iterations")
    .default_value(20)
    .scan<'i', int>();

    program.add_argument("--off-x")
    .help("X offset")
    .default_value(0.0)
    .scan<'g', double>();

    program.add_argument("--off-y")
    .help("Y offset")
    .default_value(0.0)
    .scan<'g', double>();

    program.add_argument("--scale")
    .help("image scale factor")
    .default_value(1.0)
    .scan<'g', double>();


    try {
        program.parse_args(argc, argv);
    }
    catch (const std::runtime_error& err) {
        std::cout << err.what() << std::endl;
        std::cout << program;
        exit(0);
    }

    filename = program.get<std::string>("filename");
    color = program.get<uint32_t>("--color");
    end_color = program.get<uint32_t>("--end-color");
    x_size = program.get<int>("-x");
    y_size = program.get<int>("-y");
    black_threshold= program.get<int>("-t");
    mandel_cut = program.get<int>("--cut");
    scale = program.get<double>("--scale");
    double off_x = program.get<double>("--off-x");
    double off_y = program.get<double>("--off-y");

    offset = std::tuple<double, double>(off_x, off_y);

}
