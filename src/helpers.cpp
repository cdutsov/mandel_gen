#include <helpers.hpp>
#include <sstream>
#include <math.h>

Color::Color(int r, int g, int b, double l) {
    RGB col{r, g, b};
    main_color = col;
};

RGB Color::int_to_color(int y) {
    if (y > 256 * 256 * 256) {
        y = 256 * 256 * 256;
    }
    int b = y % 256;
    int g = y / 256 % 256;
    int r = y / 256 / 256 % 256;
    RGB col{r, g, b};
    return col;
}

Color::Color(int y) {
    main_color = int_to_color(y);
};

Color::Color(int start, int end) {
    main_color = int_to_color(start);
    bgn_color = int_to_color(end);
};

RGB Color::mix_colors(double mix) {
    int new_r = (main_color.r - bgn_color.r) * mix + bgn_color.r;
    int new_g = (main_color.g - bgn_color.g) * mix + bgn_color.g;
    int new_b = (main_color.b - bgn_color.b) * mix + bgn_color.b;
    RGB new_col{new_r, new_b, new_g};
    return new_col;
};
